import React, { useState } from "react";

export default function AgregarTareas({ onNewChange, nuevaTarea }) {
  const styleLabel = "block text-sm font-medium leading-6 text-gray-900";
  const styleInput =
    "block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6";

  const [onnew, setOnnew] = useState([false, false]);

  const [datos, setDatos] = useState(["", ""]);

  const validarNuwe = () => {
    onNewChange(onnew[0] || onnew[1]);
  };

  const handleInputTareaChange = (event) => {
    const nuevoValor = event.target.value;
    console.log(nuevoValor);
    console.log(nuevoValor != "");
    
    setOnnew([nuevoValor != "", onnew[1]]);
    setDatos([nuevoValor, datos[1]]);
    validarNuwe();
  };

  const handleInputDetalleChange = (event) => {
    const nuevoValor = event.target.value;

    setOnnew([onnew[0], nuevoValor != ""]);
    setDatos([datos[0], nuevoValor]);
    validarNuwe();
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let tarea = {
      tarea: datos[0],
      detalles: datos[1],
      creado: Date.now(),
    };

    nuevaTarea(tarea);
    setOnnew([false, false]);
    setDatos(["", ""]);
    validarNuwe();
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="bg-slate-100 rounded-xl p-8 m-8 dark:bg-slate-800 shadow-2xl flex flex-col">
        <h1>Agregar Taria</h1>

        <div className="m-4">
          <label htmlFor="tarea" className={styleLabel}>
            tarea address
          </label>
          <div className="mt-2">
            <input
              id="tarea"
              name="tarea"
              type="text"
              autoComplete="text"
              required
              value={datos[0]}
              onChange={handleInputTareaChange}
              className={styleInput}
            />
          </div>
        </div>

        <div className="m-4">
          <label htmlFor="detalles" className={styleLabel}>
            detalles address
          </label>
          <div className="mt-2">
            <textarea
              id="detalles"
              name="detalles"
              autoComplete="detalles"
              value={datos[1]}
              onChange={handleInputDetalleChange}
              required
              className={styleInput}
            />
          </div>
        </div>

        <button>Agregar tarea</button>
      </div>
    </form>
  );
}
