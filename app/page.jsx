
'use client'

import AgregarTareas from "@/components/agregarTareas";
import ListarTareas from "@/components/listadoTareas";
import Image from "next/image";
import { useState } from "react";

export default function Home() {
  const [tareas, setTareas] = useState([]);
  const [newtarea, setNewtarea] = useState(false);

  const changeNueTares = (estado) => setNewtarea(estado);

  const addTareas = (tarea) => {
    setTareas([...tareas, tarea]);
  };


  return (
    <main className="flex min-h-screen flex-row justify-center  p-24">
      <AgregarTareas onNewChange={changeNueTares} nuevaTarea={addTareas} />
      <ListarTareas creando={newtarea} lista={tareas} />
    </main>
  );
}
